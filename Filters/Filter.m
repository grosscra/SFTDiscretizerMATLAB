classdef (Abstract) Filter < handle & matlab.mixin.Heterogeneous
%Filter- Abstract class for filters use to discretize nonequispaced signals.
%The filters are assumed to be 1-periodic. Their main purpose is to approximate
%values of discrete signal an nonequispaced points via semi-discrete 
%
% Filter Properties:
%	modulation: Fourier space shift.
%	frequencyCutoff: Effective bandwidth of filter.
%	convolutionCutoff: Effective spatial width of filter.
%
% Filter Methods:
%	evaluate: Evaluate the filter.
%	fourierTransform: Fourier transform of filter.
%
% Author: Craig Gross
% Mar 2020

	properties 
		% modulation: A Fourier-space shift of the filter
		modulation {mustBeInteger}
	end
		
	properties (SetAccess = private)
		% frequencyCutoff: The effective bandwidth of the filter,
		% that is, most significant frequency content lies in 
		% [-frequencyCutoff, frequencyCutoff]. 
		frequencyCutoff {mustBeInteger, mustBePositive}
		% convolutionCutoff: Determines how many signal samples should be 
		%	used in a convolution
		convolutionCutoff {mustBeInteger, mustBePositive}
	end

	methods (Abstract)
		%EVALUATE- Evaluate the filter at given points.
		%
		% Syntax:	[ y ] = evaluate(filter, x)
		%      or   [ y ] = filter.evaluate(x)
		%
		% Inputs:
		%    x: Matrix of points where filter is evaluated. Since the filters 
		%		are used for convolutions, the evaluate method must be 
		%		vectorized to accept and output a matrix of the same size 
		%		(which is used for the the various differences in the 
		%		convolutions).
		%
		% Outputs:
		%    y: The value of the filter at each point in x.
		%    
		[ y ] = evaluate(filter, x)
		%FOURIERTRANSFORM- Fourier transform of filter at desired frequencies.
		%
		% Syntax:	[ filterHat ] = fourierTransform(filter, omega)
		%      or   [ filterhat ] = filter.fourierTransform(omega)
		%
		% Inputs:
		%    omega: Frequencies to evaluate the Fourier transform of the filter.
		%
		% Outputs:
		%    filterHat: The value of the Fourier transform of the filter at the 
		%		given frequencies.
		%
		% NOTE: The Fourier transform convention used is
		%			int_0^1 filter(x) * exp(-2 * pi * i * omega * x) dx
		%    
		[ filterHat ] = fourierTransform(filter, omega)
	end
	methods
		%function filter = Filter(frequencyCutoff, convolutionCutoff, N, convolutionPoints)
		function filter = Filter(frequencyCutoff, convolutionCutoff)
		%FILTER: Main constructor.
		%Syntax: filter = Filter(frequencyCutoff, convolutionCutoff)
			filter.modulation = 0;
			filter.frequencyCutoff = ceil(frequencyCutoff);
			filter.convolutionCutoff = convolutionCutoff;
		end
	end
end

