function [ convolution ] = executeConvolution(N, modulation, filterPoints, filterValues, f, rows, cols)
			convolution = (1 / N) .* sum( exp(2i .* pi .* modulation .* filterPoints) .* filterValues .* f, 2);
end
