classdef GaussianFilter < Filter
%GaussianFilter- Filter implementation for periodized gaussians.
%All parameters are determined by an underlying alpha and beta. Alpha is used to specify the effective bandwidth of the gaussian and beta is used to specify the main coefficient. See [Mehri et al., 2019] for more information.
%
% GaussianFilter Properties:
%	modulation: Fourier space shift.
%	frequencyCutoff: Effective bandwidth of filter.
%	convolutionCutoff: Effective spatial cutoff in terms of convolution.
%	N: Length of signals the filter is used for.
%	C: Main coefficient.
% GaussianFilter Methods:
%	evaluate: Evaluate the filter.
%	fourierTransform: Fourier transform of filter.
%
% Author: Craig Gross
% Mar 2020

	properties (Access = private)
		% N: Length of signals the filter is used for.
		%N {mustBeInteger, mustBePositive};
		% mesh: Row vector representing N discretization of [0, 1].
		% Includes 0 but not 1.
		%mesh {mustBeGreaterThanOrEqual(mesh, 0), ...
		%	mustBeLessThan(mesh, 1)}
		% C: Main coefficient of Gaussian, see [Mehri et al. 2019, Eq. (3)].
		C {mustBePositive}
		% M: The size of the sum to use in the periodization of the Gaussian.
		% See GaussianFilter.evaluate for more.
		M {mustBeInteger, mustBeNonnegative}
		% convolutionCutoff: Determines how many signal samples should be 
		%	used in a convolution
		%convolutionCutoff {mustBeInteger, mustBePositive}
	end

	methods
		%function filter = GaussianFilter(N, convolutionPoints, varargin)
		function filter = GaussianFilter(N, varargin)
		%GAUSSIANFILTER: Main constructor.
		% Syntax: filter = GaussianFilter(N)
		%	with optional Name, Value pairs for 
		%		alpha (e.g., GaussianFilter(N, 'alpha', 1))
		%		beta (e.g., GaussianFilter(N, 'beta', 1))
		%	or any combination of these Name, Value pairs.
		% 
		% Inputs:
		%	N: Length of signals convolved with the filter
		%	alpha (Optional, Default = beta * sqrt(2) / log(3 / sqrt(2 * pi)):
		%		Used to specify frequencyCutoff
		%	beta (Optional, Default = 1): Used to specify gaussian coefficient
		
			[alpha, beta] = ...
				variableInputHandler(varargin, 'alpha', 'beta');
			if isempty(beta)
				beta = 1;
			end
			% See the remark after [Mehri et al., Lemma 3]
			if isempty(alpha)
				alpha = sqrt(2) * beta / log(3 / sqrt(2 * pi));
			end

			% See [Mehri et al., Lemma 3]
			frequencyCutoff = N / (alpha .* sqrt(log(N)));
			% See [Mehri et al., Theorem 4]
			convolutionCutoff = ceil( (beta .^ 2 ./ ...
				(6 .* sqrt(2) .* pi)) .* log(N) ) + 1;
			filter@Filter(frequencyCutoff, convolutionCutoff);
			% See [Merhi et al., Lemma 3]
			filter.C = beta * sqrt(log(N)) / N;
			filter.M = 3; % Nearly same as much larger sum to double precision.

		end

		function [ y ] = evaluate(filter, x)
		%EVALUATE- Evaluate the Gaussian filter at given points.
		%See Filter.evaluate.
		%NOTE: The periodized Gaussian is defined with an infinte 
		%	sum. We truncate that sum in either at filter.M terms.
			reshapeDims = [ones(1, ndims(x)), 2 * filter.M + 1];
			y = exp(2i .* pi .* filter.modulation .* x) .* ...
				sum(exp(-2 .* (pi .* (mod(x, 1) - ...
				reshape(-filter.M:filter.M, reshapeDims))).^2 ...
				./ (filter.C .^ 2)), ndims(x) + 1)	./ filter.C;
		end

		function [ filterHat ] = fourierTransform(filter, omega)
		%FOURIERTRANSFORM- Fourier transform of filter at desired frequencies.
		%See Filter.FourierTransform.
			filterHat = exp(-1 .* filter.C.^2 .* ...
				(omega - filter.modulation).^2 ./ 2) ./ sqrt(2 .* pi);
		end

	end

end

function [ varargout ] = variableInputHandler(inputs, varargin)
%VARIABLEINPUTHANDLER- Extracts Name, Value inputs from a varargin cell array
%In addition to the inputs cell array of Name, Value pairs, accepts strings 
%corresponding to names to extract from the cell array.
%
% Syntax:  [ varargout ] = variableInputHandler(inputs, ...)
%     with optional trailing string arguments.
%
% Inputs:
%    inputs: A one-dimensional cell array containing strings/character vectors 
%		representing names followed by the value of that property.
%    varargin: Optional strings representing the names of properties to be 
%		extracted from inputs.
%
% Outputs:
%    varargout: For each name given in varargin, will output the 
%		corresponding value as given in inputs. If the name does not exist in 
%		inputs, an empty array will be returned in that position. Checks should 
%		be made for empty arrays in order to assign default values if necessary.

% Author: Craig Gross
% Mar 2020

if nargout ~= length(varargin)
	error('Number of outputs must match number of names input.');
end

names = cell(1, length(inputs) / 2);
for i = 1:2:length(inputs)
	if isstring(inputs{i})
		names{(i + 1) / 2} = char(inputs{i});
	elseif ischar(inputs{i})
		names{(i + 1) / 2} = inputs{i};
	else
		error('Name of input should be character array or string.');
	end
end

for i = 1:length(varargin)
	namesIndex = find(contains(names, varargin{i}));
	if isempty(namesIndex)
		varargout{i} = []; 
	else
		varargout{i} = inputs{2 * namesIndex};
	end
end

end
