classdef FilterConvolver < handle
%FilterConvolver- Handles convolutions with filters.
%Approximates convolutions at a given series of points using truncated 
%semi-discrete convolution and information derived from a Filter.
%
% FilterConvolver Properties:
%	N: Length of the convolution.
%	filter: A Filter object to convolve with.
%	convolutionPoints: All points where the convolution will be evaluated.
%	modulation: Fourier space shift of filter.
%	necessaryIndices: Indices of a discrete signal to use in each convolution.
%	convolutionIndices: necessaryIndices arranged for convolution evaluation.
%
% Filter Methods:
%	execute: Convolve the filter with the signal.
%
% Author: Craig Gross
% May 2020

	properties 
		% modulation: A Fourier-space shift of the filter
		modulation {mustBeInteger}
	end

	properties (SetAccess = private)
		% N: Length of the convolution and any signal being convolved.
		N {mustBeInteger, mustBePositive}
		% necessaryIndices: The indices of entries in a given signal which
		%	are necessary for the convolution calculation. This is the least 
		%	amount of information necessary from a discrete signal to perform 
		%	the desired convolution.
		necessaryIndices {mustBeInteger, mustBePositive}
		% convolutionIndices: The indices in necessary arranged in a matrix
		%   where each row corresponds to a point where the convolution is 
		%   evaluated and each column corresponds to the length of the sum 
		%   in each convolution. Fastest to give the convolution execution 
		%   method a signal pre-indexed at convolutionIndices.
		convolutionIndices {mustBeInteger, mustBePositive}
	end

	properties (Access = private)
		% filter: A Filter object to convolve with input signals. This should
		%	effectively act as a bandpass filter.
		filter Filter
		% convolutionPoints: A column vector of points in [0, 1] representing 
		%	the points where the convolution will evaluated.
		convolutionPoints {mustBeGreaterThanOrEqual(convolutionPoints, 0), ...
			mustBeLessThanOrEqual(convolutionPoints, 1)}
		% filterPoints: Points where the filter is evaluated in convolution
		filterPoints {mustBeGreaterThanOrEqual(filterPoints, 0), ...
			mustBeLessThanOrEqual(filterPoints, 1)}
		% filterValues: Precomputed evaluation of filter at filterPoints
		filterValues {mustBeNumeric}
		% converter: Used to convert signals indexed at necessaryIndices 
		%   to those in convolutionIndices
		converter {mustBeInteger, mustBePositive}
	end
		
	methods 
		function convolver = FilterConvolver(N, filter, convolutionPoints)
			%FilterConvolver: Main constructor.
			%Syntax: convolver = FilterConvolver(N, filter, convolutionPoints)
			
			convolver.N = N;
			convolver.filter = filter;
			convolver.convolutionPoints = convolutionPoints;
			convolver.modulation = 0;

			% Setup the truncated convolution on the mesh 0:(N - 1) ./ N
			% First find the mesh points closest to the desired convolution pts
			nearestMeshPoints = mod(round(convolver.N .* ...
				convolver.convolutionPoints) ./ convolver.N, 1);
			% Then center the convolution here and truncate based on spatial 
			% width of the filter
			truncatedPoints = mod(nearestMeshPoints + ...
				(-filter.convolutionCutoff:filter.convolutionCutoff) ...
				./ convolver.N, 1);
			% Record the points where the filter is evaluated for later 
			%	modulation
			convolver.filterPoints = mod(convolver.convolutionPoints - ...
				truncatedPoints, 1);
			% Precompute the values of the filter for every convolution
			convolver.filterValues = filter.evaluate(convolver.filterPoints);
			% Now get the indices of the signal at the mesh points
			convolver.convolutionIndices = round(convolver.N .* truncatedPoints) + 1;
			% Consolidate this matrix into the least amount of information
			[convolver.necessaryIndices, ~, convolver.converter] =...
				unique(convolver.convolutionIndices);
		end

		function [ convolver ] = set.modulation(convolver, modulation)
			% Property set method for modulation to ensure that the 
			%	filter's modulation is synced up.
			convolver.modulation = modulation;
			convolver.filter.modulation = modulation;
		end

		function [ readyToConvolve ] = signalFormat(convolver, f)
		%SIGNALFORMAT- Format various signal types for convolution.
		%Accepts two lengths of signals, and outputs the matrix of 
		%signal values expected for use with EXECUTE method.
		%
		% Syntax:  [ readyToConvolve ] = signalFormat(convolver, f)
		%      or  [ readyToConvolve ] = convolver.signalFormat(f)
		%
		% Inputs:
		%    f: Either:
		%		1) A discrete, length N signal over [0, 1] **pre-indexed** at 
		%			the indices in convolver.necessaryIndices OR
		%		2) Length N column vector representing equispaced samples of a 
		%			1-periodic signal. The samples are assumed to be taken at 
		%			{j / N} for j = 0, ..., N - 1.
		%
		% Outputs:
		%    readyToConvolve: The values in f repeated and reshaped into the 
		%      matrix format expected by EXECUTE, i.e., a standard discrete 
		%      signal indexed at the indices in convolutionIndices.
			if(length(f) == convolver.N)
				readyToConvolve = f(convolver.convolutionIndices);
			elseif(length(f) == length(convolver.necessaryIndices))
				readyToConvolve = reshape(f(convolver.converter), ...
					size(convolver.convolutionIndices));
			else
				error(...
					sprintf(...
					'The given signal cannot be used to perform a convolution. Must be length %d or length %d.',...
					convolver.N, convolver.necessaryIndices));
			end
		end

		function [ convolution ] = execute(convolver, f)
		%CONVOLVE- Semi-discrete conv of signal with the filter at specified pts.
		%In particular the points are given during the plan's creation and 
		%reused throughout.
		%
		% Syntax:  [ convolution ] = execute(convolver, f)
		%      or  [ convolution ] = convolver.execute(f)
		%
		% Inputs:
		%    f: Either:
		%		1) A discrete, length N signal over [0, 1] **pre-indexed** at 
		%			the indices in convolver.convolutionIndices OR
		%		2) A discrete, length N signal over [0, 1] **pre-indexed** at 
		%			the indices in convolver.necessaryIndices OR
		%		3) Length N column vector representing equispaced samples of a 
		%			1-periodic signal. The samples are assumed to be taken at 
		%			{j / N} for j = 0, ..., N - 1.
		%
		% Outputs:
		%    convolution: Column vector of approximations to semi-discrete 
		%		convolution evaluated at each input x of the form
		%		(1/N) sum_j( filter(x - j / N) .* f(j) ).
		%		To approximate, the full sum over j is not used, only the 
		%		indices corresponding to significant frequency values as 
		%		determined by convolutionCutoff
		
			if(~all(size(f) == size(convolver.convolutionIndices)))
				g = convolver.signalFormat(f);
			else
				g = f;
			end
			convolution = (1 / convolver.N) .* sum( exp(2i .* pi .* ...
				mod(convolver.modulation .* convolver.filterPoints, 1)) .* ...
				convolver.filterValues .* g, 2);
		end

	end
end

