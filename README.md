# SFT Discretizer (for MATLAB)

This project is a MATLAB based implementation of the discretization process for sparse Fourier transforms (SFT) given in Algorithm 1 of [Mehri et al., 2019]. 

Given a discrete signal and an SFT algorithm which requires signal information outside of the discretization, the algorithm produces evaluations of filtered versions of the signal at the SFT's required sampling points which can then be used to determine the frequency content of the original signal.

This code is meant to be flexible for use with _any_ nonequispaced sparse Fourier transform. As such, the algorithm is implemented via functions which interface with an SFT only through the necessary nonequispaced sampling points and the sparse Fourier approximations output. In particular, as in [Mehri et al., 2019] we use the SFT algorithm from [Iwen, 2013] for testing which is implemented for MATLAB at grosscra/SublinearSparseFourierMATLAB>.

## Usage

After downloading the repository, the code is split into two main parts, `Filters` and `DSFTs`.

### `DSFTs`: The `DSFTPlan` and `DMSFTPlan` classes

If you wish to get started right away doing some discrete SFTs, use the `DMSFTPlan` which makes use of the SFT code from grosscra/SublinearSparseFourierMATLAB>. Make sure you have that code downloaded and the `SFTPlan` object is available on your path.

The general work flow (as with `SFTPlan`) is to create a plan using the constructor `plan = DMSFTPlan(N, s, executionMode, ...)` where `N` is the length of the signal, `s` is the sparsity, and `executionMode` is a string for either deterministic or random execution. Finally, run `[coefficients, frequencies] = plan.execute(signal)` to obtain the sparse discrete Fourier approximation to a length `N` signal.

The `DSFTPlan` class is a more general implementation of the discretization algorithm which allows you to plug in your own SFT code (see how `DMSFTPlan` inherits from `DSFTPlan` for an example). In order to create a `DSFTPlan` object, you need to supply
* The sparsity `s`
* A Filter object `filter` (see the next section),
* The sampling nodes for your SFT algorithm `nodes`,
* And a function `execute` which takes function samples at those nodes and produces a SFT with the syntax `[coefficients, frequencies] = execute(samples)`

to the constructor `plan = DSFTPlan(s, filter, nodes, execute)`, and run it as with `DMSFT` above.

The documentation for these classes, their constructors, and `DSFTPlan.execute` are good places to start. Default parameters are chosen to satisfy theoretical accuracy guarantees, but can usually be decreased a good amount for better runtime without sacrificing much accuracy. Experiment at your own risk!

### `Filters`: The `Filter` class

The use of filters is modularized into its own abstract class `Filter`. The `DSFTPlan` above uses a `Filter` object to do all of its "bandpassing". In particular, `DMSFTPlan` makes use of a gaussian filter implemented as a conrete sublcass `GaussianFilter` as in [Mehri et al., 2019]. If you wish to use your own filters, the documentation of `Filter` to know what a filter must be able to do (e.g., you should be able to evaluate it, take its Fourier transform, convolve with it, ...), and the documentation for `GaussianFilter` and its implementation in `DMSFTPlan` for examples.

### Tests

For examples of the `DMSFTPlan` in action, see the `Tests` directory. Tests will automatically add the `Filters` and `DSFTs` directories to the path. Again, make sure that you have `SFTPlan` downloaded and on your path.

## See also

With the SFT algorithm from the [SublinearSparseFourierMATLAB repository](https://gitlab.com/grosscra/SublinearSparseFourierMATLAB) mentioned above, this code is a reimplementation of DMSFT in the [MSU Sparse Fourier Repository](https://sourceforge.net/projects/aafftannarborfa/), originally implemented by Ruochuan Zhang and evaluated in [Mehri et al., 2019].

## References

* [Sami Mehri](https://users.math.msu.edu/users/merhisam/), Ruochuan Zhang, [Mark Iwen](https://users.math.msu.edu/users/iwenmark/index.html), and [Andrew Christlieb](http://www.the-christlieb-group.org/), **A new class of fully discrete sparse fourier transforms: faster stable implementations with guarantees**, Journal of Fourier Analysis and Applications 25(2019), pp. 751-784, [https://link.springer.com/article/10.1007/s00041-018-9616-4](https://link.springer.com/article/10.1007/s00041-018-9616-4). ([Preprint](https://users.math.msu.edu/users/iwenmark/Papers/DiscreteSFT.pdf) available on author's webpage.)
* [Mark Iwen](https://users.math.msu.edu/users/iwenmark/index.html), **Improved approximation guarantees for sublinear-time Fourier algorithms**, Applied and Computational Harmonic Analysis, 34(1)(2013), pp. 57-82, [https://www.sciencedirect.com/science/article/pii/S1063520312000462](https://www.sciencedirect.com/science/article/pii/S1063520312000462). ([Preprint](https://users.math.msu.edu/users/iwenmark/Papers/Iwen_Improved.pdf) available on author's webpage.)
