%TESTPLANPRIMEREDUCTION-  Tests DMSFTPlan and its primes.
% Parameter sweep on number of primes, number of shifts, 
% gaussian parameter, and where to start choosing primes.
% Looking for a fast, but reliable choice.
%
% NOTE: Need to use PrimeChoice branch of SublinearSparseFourierMATLAB
%
%
% Classes required: DMSFTPlan 
%
% See also: DMSFTPlan

addpath("../DSFTs");
addpath("../Filters");
warning('off', 'SFTPlan:outsideBand');
warning('off', 'SFTPlan:tooFewCoeffs');

if ~exist('primeList') || length(primeList) ~= 10000
	primeList = primes(104740); % First 10,000 primes
end

N = 2^25;
k = 100;
for C = 0.1
	for alpha = 2
		for beta = 2
			for primeShift = 40
				plan = DMSFTPlan(N, k, 'deterministic', ...
					'C', C, 'alpha', alpha, 'beta', beta, ...
					'primeList', primeList, 'primeShift', primeShift,...
					'parallelSFT', true);

				trueFreq = -ceil(N / 2) + randperm(N, k);
				trueCoeff = exp(2i * pi * rand(1, k));
				fHat = accumarray(mod(trueFreq.', N) + 1, trueCoeff, [N, 1]);
				samples = N .* ifft(fHat);

				startEx = tic;
				[approxCoeff, approxFreq] = plan.execute(samples);
				toc(startEx)

				numCorrect = length(intersect(trueFreq, approxFreq));
				if numCorrect == k
					fprintf('Good set of parameters: C=%2.3f, alpha=%2.3f, beta = %2.3f, primeShift = %d.\n', C, alpha, beta, primeShift);
				end
			end
		end
	end
end
