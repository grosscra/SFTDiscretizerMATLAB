%TESTREDUCEDSAMPLEFORMAT- Tests DMSFTPlan using a smaller number of samples.
%
% Classes required: DMSFTPlan 
% Other m-files required: trigPoly.m
%
% See also: DMSFTPlan

addpath("../DSFTs");
addpath("../Filters");

if ~exist('primeList') || length(primeList) ~= 10000
	primeList = primes(104740); % First 10,000 primes
end

% Test sampling at fewer points

N = 20000;
s = 1;
plan = DMSFTPlan(N, s, 'deterministic', 'primeList', primeList, 'C', 0.5);

fHat = zeros(N, 1);
fHat(ceil(N / 2)) = 1 + 1i; % Matches zero frequency for use in trigPoly
f = @(x) trigPoly(x, fHat);

signal = f(plan.necessaryNodes);
[coefficients, frequencies] = plan.execute(signal);

assert(any(frequencies == 0));
assert(norm(coefficients(frequencies == 0) - (1 + 1i)) < 1e-6);
disp('Test passed.');
fprintf('Uses %3.2f%% as many samples as length %d discrete sampling.\n',...
	100 * length(signal) / N, N);

% Test indexing full discrete sample to reduce data overhead

mesh = ((0:N - 1) ./ N)';

trueCoeff = [1 + 1i];
trueFreq = [-1];
f = @(x) exp(2i .* pi .* x * trueFreq) * trueCoeff.';

signal = f(mesh);
signal = signal(plan.necessaryIndices);
[coefficients, frequencies] = plan.execute(signal);

assert(any(frequencies == -1));
assert(norm(coefficients(frequencies == -1) - (1 + 1i)) < 1e-6);
disp('Test passed.');
fprintf('Uses %3.2f%% as many samples as length %d discrete sampling.\n',...
	100 * length(signal) / N, N);

% Test sampling trig polynomial 'quickly' 
% (i.e., with ifft but only keeping necessary values.)

N = 2^24;
s = 10;
plan = DMSFTPlan(N, s, 'deterministic', 'primeList', primeList, 'C', 0.5);

trueFrequencies = randperm(N, s) - (ceil(N / 2) + 1);
trueCoefficients = 2 * rand(s, 1) - 1 + 1i * (2 * rand(s, 1) - 1);

tic;
samples = plan.sampleTrigPolynomial(trueCoefficients, trueFrequencies);
[coefficients, frequencies] = plan.execute(samples);
fftTime = toc;

[~, ia, ib] = intersect(trueFrequencies, frequencies);

assert(length(ia) == length(trueFrequencies));
assert(norm(trueCoefficients(ia) - coefficients(ib).') ...
	/ norm(trueCoefficients) < 1e-2);

fprintf('FFT test passed in %4.5f seconds.\n', toc);
fprintf('Uses %3.2f%% as many samples as length %d discrete sampling.\n',...
	100 * length(samples) / N, N);

tic;
samples = exp(2i .* pi .* plan.necessaryNodes * trueFrequencies) * trueCoefficients;
[coefficients, frequencies] = plan.execute(samples);
explicitSamplingTime = toc;

[~, ia, ib] = intersect(trueFrequencies, frequencies);

assert(length(ia) == length(trueFrequencies));
assert(norm(trueCoefficients(ia) - coefficients(ib).') ...
	/ norm(trueCoefficients) < 1e-2);

fprintf('Explicit sampling test passed in %4.5f seconds.\n', toc);
fprintf('Uses %3.2f%% as many samples as length %d discrete sampling.\n',...
	100 * length(samples) / N, N);

% 
% 
% % Test function with two small frequencies
% 
% N = 33;
% mesh = ((0:N - 1) ./ N)';
% s = 2;
% plan = DMSFTPlan(N, s, 'deterministic', 'primeList', primeList, 'C', 8);
% 
% trueCoeff = [exp(2i * pi * (1/8)), exp(2i * pi * (2/3))];
% trueFreq = [0, 1];
% f = @(x) exp(2i .* pi .* x * trueFreq) * trueCoeff.';
% 
% signal = f(mesh);
% [coefficients, frequencies] = plan.execute(signal);
% 
% [recovered, locations] = ismembertol(trueFreq, frequencies);
% assert(nnz(recovered) == s);
% assert(norm(trueCoeff - coefficients(locations)) < 1e-8);
% disp('Two small frequencies with user sampling test passed');
% 
% % Test function with two small non-positive frequencies
% 
% trueCoeff = [exp(2i * pi * (1/8)), exp(2i * pi * (2/3))];
% trueFreq = [-1, -2];
% f = @(x) exp(2i .* pi .* x * trueFreq) * trueCoeff.';
% 
% signal = f(mesh);
% [coefficients, frequencies] = plan.execute(signal);
% 
% [recovered, locations] = ismembertol(trueFreq, frequencies);
% assert(nnz(recovered) == s);
% assert(norm(trueCoeff - coefficients(locations)) < 1e-8);
% disp('Two small nonpositive frequencies with user sampling test passed');
% 
% % Test function with two mixed sign frequencies
% 
% trueCoeff = [exp(2i * pi * 13), exp(2i * pi * 74)];
% trueFreq = [3, -16];
% f = @(x) exp(2i .* pi .* x * trueFreq) * trueCoeff.';
% 
% signal = f(mesh);
% [coefficients, frequencies] = plan.execute(signal);
% 
% [recovered, locations] = ismembertol(trueFreq, frequencies);
% assert(nnz(recovered) == s);
% assert(norm(trueCoeff - coefficients(locations)) < 1e-8);
% disp('Two mixed sign frequencies with user sampling test passed');
