%TESTDETPLANRANDOMPOLYNOMIALS- Tests deterministic DMSFTPlan with random poly.
%
% Classes required: DMSFTPlan
%
% See also: DMSFTPlan

% Author: Craig Gross
% Mar 2020

addpath("../DSFTs");
addpath("../Filters");
warning('off', 'SFTPlan:outsideBand');

if ~exist('primeList') || length(primeList) ~= 10000
	primeList = primes(104740); % First 10,000 primes
end

numTrials = 10;

for N = 2 .^ (4:10)
	mesh = ((0:N - 1) ./ N)';
	for s = 1:ceil(N / 4)
		plan = DMSFTPlan(N, s, 'deterministic', 'primeList', primeList, ...
			'beta', 1, 'alpha', 1);
		for i = 1:numTrials
			trueFreq = -ceil(N / 2) + randperm(N, s);
			trueCoeff = exp(2i * pi * rand(1, s));
			f = @(x) exp(2i .* pi .* x * trueFreq) * trueCoeff.';
			signal = f(mesh);
			[approxCoeff, approxFreq] = plan.execute(signal);
			[recovered, locations] = ismembertol(trueFreq, approxFreq);
			assert(nnz(recovered) == s);
			assert(norm(trueCoeff - approxCoeff(locations)) < 1e-5)
		end
		fprintf('Passed test with N = %d, s = %d, %d times.\n', N, s, numTrials);
	end
end

