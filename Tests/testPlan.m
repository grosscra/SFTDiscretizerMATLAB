%TESTPLAN- Tests DMSFTPlan with some simple polynomials.
%
% Classes required: DMSFTPlan 
% Other m-files required: trigPoly.m
%
% See also: DMSFTPlan

addpath("../DSFTs");
addpath("../Filters");

if ~exist('primeList') || length(primeList) ~= 10000
	primeList = primes(104740); % First 10,000 primes
end

% Test constant function

N = 100;
mesh = ((0:N - 1) ./ N)';
s = 1;
plan = DMSFTPlan(N, s, 'deterministic', 'primeList', primeList);

fHat = zeros(N, 1);
fHat(ceil(N / 2)) = 1 + 1i; % Matches zero frequency for use in trigPoly
f = @(x) trigPoly(x, fHat);

signal = f(mesh);
[coefficients, frequencies] = plan.execute(signal);

assert(any(frequencies == 0));
assert(norm(coefficients(frequencies == 0) - (1 + 1i)) < 1e-8);
disp('Constant function with user sampling test passed');

% Test one negative frequency

N = 80;
mesh = ((0:N - 1) ./ N)';
s = 1;
plan = DMSFTPlan(N, s, 'deterministic', 'primeList', primeList);

trueCoeff = [1 + 1i];
trueFreq = [-1];
f = @(x) exp(2i .* pi .* x * trueFreq) * trueCoeff.';

signal = f(mesh);
[coefficients, frequencies] = plan.execute(signal);

assert(any(frequencies == -1));
assert(norm(coefficients(frequencies == -1) - (1 + 1i)) < 1e-8);
disp('One negative frequency with user sampling test passed');


% Test function with two small frequencies

N = 33;
mesh = ((0:N - 1) ./ N)';
s = 2;
plan = DMSFTPlan(N, s, 'deterministic', 'primeList', primeList, 'C', 8);

trueCoeff = [exp(2i * pi * (1/8)), exp(2i * pi * (2/3))];
trueFreq = [0, 1];
f = @(x) exp(2i .* pi .* x * trueFreq) * trueCoeff.';

signal = f(mesh);
[coefficients, frequencies] = plan.execute(signal);

[recovered, locations] = ismembertol(trueFreq, frequencies);
assert(nnz(recovered) == s);
assert(norm(trueCoeff - coefficients(locations)) < 1e-8);
disp('Two small frequencies with user sampling test passed');

% Test function with two small non-positive frequencies

trueCoeff = [exp(2i * pi * (1/8)), exp(2i * pi * (2/3))];
trueFreq = [-1, -2];
f = @(x) exp(2i .* pi .* x * trueFreq) * trueCoeff.';

signal = f(mesh);
[coefficients, frequencies] = plan.execute(signal);

[recovered, locations] = ismembertol(trueFreq, frequencies);
assert(nnz(recovered) == s);
assert(norm(trueCoeff - coefficients(locations)) < 1e-8);
disp('Two small nonpositive frequencies with user sampling test passed');

% Test function with two mixed sign frequencies

trueCoeff = [exp(2i * pi * 13), exp(2i * pi * 74)];
trueFreq = [3, -16];
f = @(x) exp(2i .* pi .* x * trueFreq) * trueCoeff.';

signal = f(mesh);
[coefficients, frequencies] = plan.execute(signal);

[recovered, locations] = ismembertol(trueFreq, frequencies);
assert(nnz(recovered) == s);
assert(norm(trueCoeff - coefficients(locations)) < 1e-8);
disp('Two mixed sign frequencies with user sampling test passed');
