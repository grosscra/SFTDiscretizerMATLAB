%TESTRANDOMPLANRANDOMPOLYNOMIALSLARGEN- Tests random DMSFTPlan with large N. 
%
% Classes required: DMSFTPlan
%
% See also: SFTPlan

% Author: Craig Gross
% Mar 2020

addpath("..");
addpath("../DSFTs");
addpath("../Filters");

warning('OFF', 'SFTPlan:outsideBand');
warning('OFF', 'SFTPlan:tooFewCoeffs');

if ~exist('primeList') || length(primeList) ~= 10000
	primeList = primes(104740); % First 10,000 primes
end

numTrials = 5;
tol = 1e-4;

for N = 2 .^ (16:2:26)
	mesh = ((0:N - 1) ./ N)';
	for s = 1:ceil(N / 2^13);
		failures = 0;
		% Pushing coefficients to limits
		plan = DMSFTPlan(N, s, 'random', 'primeList', primeList, ...
			'C', 0.5, 'sigma', 0.1, 'beta', 1, 'alpha', 1');
		for i = 1:numTrials
			trueFreq = -ceil(N / 2) + randperm(N, s);
			trueCoeff = exp(2i * pi * rand(1, s));
			f = @(x) exp(2i .* pi .* x * trueFreq) * trueCoeff.';
			signal = f(mesh);
			[approxCoeff, approxFreq] = plan.execute(signal);
			[recovered, locations] = ismembertol(trueFreq, approxFreq);
			if nnz(recovered) ~= s
				disp('Not all frequencies recovered.');
				failures = failures + 1;
			elseif norm(trueCoeff - approxCoeff(locations)) > tol
				fprintf('Frequencies recovered, but L^2 error in coefficients surpasses %e\n',...
					tol);
				failures = failures + 1;
			end
		end
		fprintf('For N = %d, s = %d, out of %d trials, %d failed.\n\n', N, s,...
			numTrials, failures)
	end
end
