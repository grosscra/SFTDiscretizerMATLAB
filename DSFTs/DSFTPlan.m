classdef DSFTPlan < handle
%DSFTPlan- Object used for constructing sparse DFT approximation of a signal.
%This class requires some sort of interface to a sparse Fourier transform 
%algorithm which takes possibly arbitrary samples from 1-periodic function and 
%returns a sparse approximation to its Fourier transform. By filtering using 
%the approach in Algorithm 1: A Generic Method for Discretizing a Given SFT 
%Algorithm from [Mehri et al., 2019] an execution of this plan requires only a 
%discrete vector (which we think of as equispaced samples on [0, 1] of the 
%aforementioned function on) and outputs a sparse approximation of its discrete 
%Fourier transform.
%
% DSFTPlan Properties:
%	s: Sparsity
%	filter: Filter to handle arbitrary sampling
%	convolver: FilterConvolver to handle fast convolutions with the filter
%	SFTExecute: A function which produces sparse Fourier approximation
%	necessaryIndices: Only indices used from discrete signal
%	necessaryNodes: Only sampling points used from a function
%
% DSFTPlan Methods:
%	execute: Run the plan and produce a DFT approximation for the signal
%
% Author: Craig Gross
% Mar 2020

	properties (SetAccess = private)
		% necessaryIndices: The indices corresponding to the only values of the 
		% discrete signal necessary to perform the SFT.
		necessaryIndices {mustBeInteger, mustBePositive}
		% necessaryNodes: The sampling points corresponding to the only values 
		% of a sampled function over [0, 1] which are necessary to perform the SFT.
		necessaryNodes {mustBeGreaterThanOrEqual(necessaryNodes, 0),...
			mustBeLessThanOrEqual(necessaryNodes, 1)}
	end

	properties (Access = protected)
		% s: sparsity of desired DFT approximation.
		s {mustBeInteger, mustBePositive}
		% filter: A Filter object. This should effectively act as a bandpass 
		% filter.
		filter Filter
		% convolver: A FilterConvolver object. This will contain the filter to
		% and action to convolve against discrete signals and produce input
		% into a non-equispaced SFT
		convolver FilterConvolver
		% SFTExecute: A function handle which accepts function samples on 
		% [0, 1] as a column vector and produces a sparse Fourier transform 
		% approximation as a pair of coefficient and frequency vectors. The 
		% syntax must be of the form
		%	[coefficients, frequencies] = SFTExecute(samples)
		% with samples the same size as nodes, and each element in coefficients 
		% being the corresponding Fourier coefficient to the frequency in 
		% frequencies. If you wish to your SFT code here, you must provide this 
		% format for the interface.
		SFTExecute function_handle
	end

	methods
		function plan = DSFTPlan(s, filter, convolver, SFTExecute)
			%DSFTPlan: Main constructor.
			%Syntax: plan = DSFTPlan(s, convolver, SFTExecute)
			
			plan.s = s;
			plan.filter = filter;
			plan.convolver = convolver;
			plan.SFTExecute= SFTExecute;
			plan.necessaryIndices = plan.convolver.necessaryIndices;
			plan.necessaryNodes = (plan.necessaryIndices - 1) ...
				./ plan.convolver.N;
		end

		function [coefficients, frequencies] = execute(plan, signal)
		%DSFTPLAN- Sparse DFT approximation of a signal.
		%
		% Syntax: [coefficients, frequencies] = execute(plan, signal)
		%      or [coefficients, frequencies] = plan.execute(signal)
		%
		% Inputs:
		%	signal: Either:
		%	  1) Length N column vector of N equispaced samples of a signal on [0, 1] OR
		%	  2) Only the necessary values from a signal in the above form. In particular,
		%	    any signal sampled at necessaryNodes, or a discrete signal indexed at 
		%	    necessaryIndices.
		%
		% Outputs:
		%    coefficients: Approximations of the s most energetic DFT entries.
		%    frequencies: The s frequencies corresponding to the output 
		%		coefficients. The frequencies are given by their canonical 
		%		values in the interval -ceil(N / 2) + 1:floor(N / 2). To obtain 
		%		0-indexed discrete frequencies with bandwidth N, apply 
		%		mod(frequencies, N).
		
		N = plan.convolver.N;
		frequencies = [];
		coefficients = [];
		% Pre-index signal at values necessary for convolution.
		% Rather than taking advantage of FilterConvolver.execute's ability 
		% to handle any format signal, we pre-index into matrix form once and 
		% for all.
		%f = signal(plan.convolver.convolutionIndices);
		f = plan.convolver.signalFormat(signal);
		for j = 1: ceil(N / (2 * plan.filter.frequencyCutoff))
			%tic
			% Set filter shift. We shift enough times above to cover
			% the entire band -ceil(N / 2) + 1: floor(N / 2).
			q = -ceil(N / 2) + 1 + (2 * j - 1) * plan.filter.frequencyCutoff; 
			plan.convolver.modulation = q;
			%fprintf('setting params: %1.3f\n', toc); tic;

			% Get the filtered function at the SFT's sampling nodes
			samples = plan.convolver.execute(f);
			%fprintf('convolution: %1.3f\n', toc); tic;

			% Get SFT of filtered function
			[filteredCoefficients, filteredFrequencies] = ...
				plan.SFTExecute(samples);
			%fprintf('SFT: %1.3f\n', toc); tic;

			% Keep only frequencies in the frequency band
			inBand = filteredFrequencies > -ceil(N / 2) ...
				& filteredFrequencies <= floor(N / 2);
			% Keep only frequencies that this filter covers
			inFilterRange = inBand & ...
				(filteredFrequencies >= q - plan.filter.frequencyCutoff ...
				& filteredFrequencies < q + plan.filter.frequencyCutoff);
			%fprintf('Checking freqs: %1.3f\n', toc); tic;
			frequencies = [frequencies, ...
				filteredFrequencies(inFilterRange)]; %#ok<AGROW>
			% Divide out by filter's Fourier coefficients
			coefficients = [coefficients, ...
				filteredCoefficients(inFilterRange) ./ ...
				plan.filter.fourierTransform(...
				filteredFrequencies(inFilterRange))]; %ok<AGROW>
			%fprintf('Adding to list: %1.3f\n', toc);
		end

		%tic
		% Take 2s largest (if possible)
		[~, sortIndices] = sort(coefficients, 'descend');
		coefficients = coefficients(...
			sortIndices(1:min(length(coefficients), 2 * plan.s)));
		frequencies = frequencies(...
			sortIndices(1:min(length(coefficients), 2 * plan.s)));
		%fprintf('Only including largest s: %1.3f\n', toc);

		end
	end
end

