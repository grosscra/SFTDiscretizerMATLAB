classdef DMSFTPlan < DSFTPlan
%DMSFTPlan- Object used for constructing sparse DFT approximation of a signal.
%Implementation of DSFTPlan using sublinear-time SFT from [Iwen, 2013] as 
%considered in [Mehri et al., 2019].
%
%NOTE: The SFT implementation is from 
%https://gitlab.com/grosscra/SublinearSparseFourierMATLAB . It is assumed that 
%this repository (or at least the SFTPlan object) is available on your MATLAB 
%path.
%
%NOTE: It is assumed that you are using the Parallelize branch of the above repo.
%
% DMSFTPlan Properties:
%	s: Sparsity
%	filter: Filter to handle arbitrary sampling
%	convolver: FilterConvolver to handle arbitrary sampling
%	SFTExecute: A function which produces sparse Fourier approximation
%	N: Bandwidth/signal length
%	nonDiscretizedPlan: Instance of SFTPlan used for SFT evaluation
%	parallelSFT: Whether or not to use a parallel SFT execution
%
% DMSFTPlan Methods:
%	execute: Run the plan and produce a DFT approximation for the signal
%
% Author: Craig Gross
% Mar 2020

	properties (Access = private)
		% N: Bandwidth/signal length
		N {mustBeInteger, mustBePositive}
		% nonDiscretizedPlan: An SFTPlan which we use for running all SFTs.
		nonDiscretizedPlan SFTPlan
		% parallelSFT: A boolean stating whether or not to use parallel
		%	execution for each of the nonequispaced SFTs.
		parallelSFT {mustBeNumericOrLogical}
	end

	methods
		function plan = DMSFTPlan(N, s, executionMode, varargin)
			%DSFTPlan: Main constructor.
			%Syntax: plan = DMSFTPlan(N, k, executionMode, ...)
			%	with optional Name, Value pairs for 
			%		C (e.g., DMSFTPlan(N, k, executionMode 'C', 4))
			%		primeList (e.g., DMSFTPlan(N, k, executionMode,...
			%			'primeList',primeList))
			%		sigma (e.g., DMSFTPlan(N, k, executionMode, 'sigma', 3/4))
			%		alpha (e.g., DMSFTPlan(N, k, executionMode, 'alpha', 1))
			%		beta (e.g., DMSFTPlan(N, k, executionMode, 'beta', 1))
			%		parallelSFT (e.g., DMSFTPlan(N, k, executionMode,...
			%			'parallel', true);
			%	or any combination of these Name, Value pairs. See SFTPlan 
			%	for explanation of the first three, GaussianFilter for 
			%	explanation of the alpha and beta, and above for parallelSFT.
			
			nonDiscretizedPlan = SFTPlan(N, s, executionMode, varargin{:});

			parallelSFT = variableInputHandler(varargin, 'parallelSFT');
			if isempty(parallelSFT)
				parallelSFT = false;
			end
			if parallelSFT
				SFTExecute = @nonDiscretizedPlan.mexExecute;
			else
				SFTExecute = @nonDiscretizedPlan.execute;
			end

			% Use a GaussianFilter as in [Mehri et al., 2019].
			filter = GaussianFilter(N, varargin{:});
			convolver = FilterConvolver(N, filter, nonDiscretizedPlan.nodes);
			plan@DSFTPlan(s, filter, convolver, SFTExecute);
			plan.parallelSFT = parallelSFT;
			plan.N = N;
			plan.nonDiscretizedPlan = nonDiscretizedPlan;
		end

		function [samples] = sampleTrigPolynomial(plan, coefficients, frequencies)
		%SAMPLETRIGPOLYNOMIAL- Samples a trig poly at the plan's sampling nodes.
		%Aliases frequencies and uses inverse fast Fourier transforms.
		%
		% Syntax:  [ samples ] = sampleTrigPolynomial(plan, coefficients, frequencies)
		%      or  [ samples ] = plan.sampleTrigPolynomial(coefficients, frequencies)
		%
		% Inputs:
		%    coefficients: Column vector of s fourier coefficients
		%    frequencies: Row vector of corresponding frequencies
		%
		% Outputs:
		%    samples: The samples of the given polynomial at the plan's necessary nodes.
		%
		% The technique for aliasing was taken from code by Lutz Kaemmerer.
		%
		% See also: DSFTPlan.execute

		% Author: Craig Gross (with acknowledgement to Lutz Kaemmerer).
		% May 2020

		% Alias the coefficients
		aliasedCoeffs = accumarray(mod(frequencies.', plan.N) + 1, ...
			coefficients, [plan.N, 1]);
		
		samples = plan.N .* ifft(aliasedCoeffs);
		samples = samples(plan.necessaryIndices);

		end
	end
end

function [ varargout ] = variableInputHandler(inputs, varargin)
%VARIABLEINPUTHANDLER- Extracts Name, Value inputs from a varargin cell array
%In addition to the inputs cell array of Name, Value pairs, accepts strings 
%corresponding to names to extract from the cell array.
%
% Syntax:  [ varargout ] = variableInputHandler(inputs, ...)
%     with optional trailing string arguments.
%
% Inputs:
%    inputs: A one-dimensional cell array containing strings/character vectors 
%		representing names followed by the value of that property.
%    varargin: Optional strings representing the names of properties to be 
%		extracted from inputs.
%
% Outputs:
%    varargout: For each name given in varargin, will output the 
%		corresponding value as given in inputs. If the name does not exist in 
%		inputs, an empty array will be returned in that position. Checks should 
%		be made for empty arrays in order to assign default values if necessary.

% Author: Craig Gross
% Mar 2020

if nargout ~= length(varargin)
	error('Number of outputs must match number of names input.');
end

names = cell(1, length(inputs) / 2);
for i = 1:2:length(inputs)
	if isstring(inputs{i})
		names{(i + 1) / 2} = char(inputs{i});
	elseif ischar(inputs{i})
		names{(i + 1) / 2} = inputs{i};
	else
		error('Name of input should be character array or string.');
	end
end

for i = 1:length(varargin)
	namesIndex = find(contains(names, varargin{i}));
	if isempty(namesIndex)
		varargout{i} = []; 
	else
		varargout{i} = inputs{2 * namesIndex};
	end
end

end
